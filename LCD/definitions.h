#include <EEPROM.h>
#define SCREENTYPE_HOME 1
#define SCREENTYPE_SETTINGS 2
#define SCREENTYPE_INFO 3

#define SETTINGS_TIME 4
#define SETTINGS_DATE 5
#define SETTINGS_TESTPRINTER 6
#define SETTINGS_SCALE_LIMIT 7
#define SCREENTYPE_TIMBANG 8

#define INFO_COMM 9
#define INFO_DEVICE 10

#define SCREENTYPE_TIMBANG_DONE 11
#define SETTINGS_MATNAME 12
#define SCREENTYPE_INFO_BAUDRATE 13
#define SCREENTYPE_INFO_SENSOR 14
#define SCREENTYPE_MANUAL_MODE 15

String SCREEN_HOME()
{
  String s = "";
  //s += "--------------------\n";
  s += F("--------MENU--------");
  s += F("A<Mulai       Info>C");
  s += F("#<Manual Mode       ");
  s += F("B<Settings Reprint>D");

  return s;
}

String SCREEN_SETTINGS()
{
  String s = "";
  //s += "--------------------\n";
  s += F("------SETTINGS------");
  s += F("B<Scale             ");
  s += F("A<Time              ");
  s += F("C<Material    Back>*");

  return s;
}

String SCREEN_MANUAL_MODE()
{
  String s = "";
  //s += "--------------------\n";
  s += F("-----MANUAL MODE----");
  s += F("A<Discharge         ");
  s += F("B<Stop              ");
  s += F("              Back>*");

  return s;
}

String SCREEN_SETTINGS_MATNAME(String mat)
{
  String s = "";
  String nmat = mat;
  for (int i = 0; i < 20 - mat.length(); i++)
  {
    nmat += " ";
  }
  //s += "--------------------\n";
  s += F("Kode Material Silo: ");
  s += F("[#]Hapus     Back[*]");
  s += nmat;
  s += F("              Set[D]");

  return s;
}
String SCREEN_SETTINGS_DATE()
{
  String s = "";
  //s += "--------------------\n";
  s += F("SET DATE     [*]Back");
  s += F("DATE  :             ");
  s += F("Month :             ");
  s += F("Year  :             ");

  return s;
}
void setScaleOffset(float delta)
{
  String sdelta = String(delta);
  for (int i = 0; i < 5; i++)
  {
    EEPROM.update(115 + i, (int)sdelta.charAt(i));
  }
}
float getScaleOffset()
{
  String delta = "";
  for (int i = 0; i < 5; i++)
  {
    delta += (char)EEPROM.read(115 + i);
  }
  return delta.toFloat();
}
void setScaleTolerance(float delta)
{
  String sdelta = String(delta);
  for (int i = 0; i < 5; i++)
  {
    EEPROM.update(116 + i, (int)sdelta.charAt(i));
  }
}
float getScaleTolerance()
{
  String delta = "";
  for (int i = 0; i < 5; i++)
  {
    delta += (char)EEPROM.read(116 + i);
  }
  return delta.toFloat();
}
void setStopDelta(float delta)
{
  String sdelta = String(delta);
  for (int i = 0; i < 5; i++)
  {
    EEPROM.update(110 + i, (int)sdelta.charAt(i));
  }
}
float getStopDelta()
{
  String delta = "";
  for (int i = 0; i < 5; i++)
  {
    delta += (char)EEPROM.read(110 + i);
  }
  return delta.toFloat();
}
void setMatName(String name)
{
  int ml = name.length();
  if (ml > 8)
    ml = 8;
  for (int i = 0; i < ml; i++)
  {
    EEPROM.update(100 + i, (int)name.charAt(i));
  }
}
String getMatName()
{
  String mn = "";
  for (int i = 0; i < 8; i++)
  {
    mn += (char)EEPROM.read(100 + i);
  }
  return mn;
}
String SCREEN_SETTINGS_TIME(String ctime)
{
  String s = "";
  //s += "--------------------\n";
  s += F("------SET TIME------");
  s += ctime;
  s += F(" ");
  s += F("Change time :       ");
  s += F("              Back>*");

  return s;
}

String SCREEN_SETTINGS_SCALE(float delta, float offset, float tolerance, LiquidCrystal_I2C lcd)
{
  //s += "--------------------\n";
  // lcd.print(F("Scale Set    [#]Back"));
  // lcd.setCursor(0, 1);
  // lcd.print(F("Toleransi : "));
  // lcd.print(String(offset));
  // lcd.setCursor(0, 2);
  // lcd.print(F("Offset    : "));
  // lcd.print(String(delta));
  // lcd.setCursor(0, 3);
  // lcd.print(F("Limit     : "));
  // lcd.print(String(offset));
}

String SCREEN_INFO()
{
  String s = "";
  //s += "--------------------\n";
  s += F("--------INFO--------");
  s += F("B<Silo Info         ");
  s += F("A<Baud Rate         ");
  s += F("              Back>*");

  return s;
}
String SCREEN_INFO_SENSOR()
{
  String s = "";
  //s += "--------------------\n";
  s += F("-----INFO SILO------");
  s += F("HUM :     %         ");
  s += F("TMP :     D. Celcius");
  s += F("VOL :     %   Back>*");

  return s;
}
String SCREEN_INFO_BAUDRATE()
{
  String s = "";
  //s += "--------------------\n";
  s += F("--------INFO--------");
  s += F("USB 9600,1,8,n      ");
  s += F("TBG 2400,1,7,e      ");
  s += F("              Back>*");

  return s;
}
