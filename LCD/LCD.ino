#include <SimpleTimer.h>
#include <AltSoftSerial.h>

#include <ctype.h>

#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <Keypad.h>
#include <Keypad_I2C.h>

#include <RTClib.h>

#include "definitions.h"
#define SILO_LOWER_LIMIT 50
#define SILO_UPPER_LIMIT 70

#define LIMIT0 0.15
#define LIMIT1 0.25
#define LIMIT2 0.45

#define SLAVE_ADDR 8
#define DAC_RESOLUTION 255
#define PCF8591 (0x90 >> 1)
#define I2CADDR 0x20 // 32
#define BLINK_INTERVAL 300

#define PIN_RELAY_VOUTP 3
#define PIN_RELAY_VOUT 4
#define PIN_RELAY_AUX1 12
#define PIN_RELAY_AUX2 13

#define PIN_SPD_1 3
#define PIN_SPD_2 4
#define PIN_SPD_3 5
#define PIN_SPD_4 6

#define LOW_QTY_BUZZER_PIN 13 // Same as AUX2
#define SPEED_RELAY_COUNT 4

RTC_DS1307 RTC;

AltSoftSerial wSerial;
// LCD addr = 63
LiquidCrystal_I2C lcd(0x3f, 15, 2);

SimpleTimer timer;

const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
char keys[ROWS][COLS] = {
    {'D', '#', '0', '*'},
    {'C', '9', '8', '7'},
    {'B', '6', '5', '4'},
    {'A', '3', '2', '1'}};

byte rowPins[ROWS] = {0, 1, 2, 3}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {4, 5, 6, 7}; //connect to the column pinouts of the keypad

Keypad_I2C keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS, I2CADDR, PCF8574);

/** Use Non I2C Keypad
//const byte row = 4;
//const byte col = 4;
//char keys[row][col] = {
//  {'1','2','3','A'},
//  {'4','5','6','B'},
//  {'7','8','9','C'},
//  {'*','0','#','D'}
//};
//
//byte rowPins[row] = {11,10,7,6};
//byte colPins[col] = {5,4,3,2};
//
//Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, row, col);
**/

// PTIME is used to print
// because printing value to lcd in every cycle is notpractical
int pTime = 0;
// Use 1 seconds delay in every print cycle
int printDelta = 300;

float mSpeedOffset = 0.01f;
float mTolerance = 0.00f;
float stop_delta = 0.75f;
float stop_percentage = 0.0f;
float wValue = 0.0f;
float tValue = 5000.0f;

bool zeroed = false;

String tValueBuffer = "";
String matNameBuffer = "";

float lPercentage = 0.0f;
long int time_prev = 0;
long int time_now = 0;
bool isStart = false;
bool finish = false;
bool acceptingTargetInput = false;
/**
 * Screen Position
 */
short int screen = SCREENTYPE_HOME;
short int cursorX = 0, cursorY = 0;
/**
 * Debugging!
 */
bool printWinfo = false;
/**
 * End Debugging
 */
float motorSpeed = 0;

/** 
 *  Status Print Process
 */
bool statPrinted = false;

char charInputBuffer = '\r';

int tBefore = 0;

short int dacVal = 0;

bool autoScale = false;

short int temp, hum, vol;
short date, month, year, hour, minute, second, dateInput = 1;
int i2cDiagnostic(int addr)
{
  int found = 0;
  for (short address = 1; address < 127; address++)
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    int error = Wire.endTransmission();

    if (error == 0)
    {
      if (address == addr)
      {
        found = 1;
      }
      // if (address == rtc_addr)
      // {
      //   found[2] = 1;
      // }
    }
    else if (error == 4)
    {
    }
  }
  return found;
}
char getInput()
{
  // Use LCD
  char key = keypad.getKey();

  if (key != NO_KEY)
  {
    charInputBuffer = key;
  }
  else
  {
  }

  char inpt = charInputBuffer;
  charInputBuffer = '\r';
  return inpt;
}
String slaveComm(String data)
{
  Wire.beginTransmission(SLAVE_ADDR);
  for (int i = 0; i < data.length(); i++)
  {
    Wire.write(data.charAt(i));
  }
  Wire.write('\n');
  Wire.endTransmission();
}
void setupTimer()
{
  Serial.print("Timer setup");
  timer.setInterval(3000, checkI2c);
  // timer.setInterval(printDelta, printStat);
}
void checkI2c()
{
  Wire.requestFrom(SLAVE_ADDR, 20);

  if (Wire.available())
  {                                           // slave may send less than requested
    String resp = Wire.readStringUntil('\n'); // receive a byte as character
    if (resp.indexOf("cs:") >= 0)
    {
      invokeSerialCommand(resp.substring(3));
      return;
    }
    vol = resp.substring(2, 7).toInt();
    // Sensor sent distance data as percentage already
    // vol = 100 - int(float(vol) / 50.0 * 100.0);
    hum = resp.substring(9, 13).toInt();
    temp = resp.substring(15).toInt();

    if (screen == SCREENTYPE_INFO_SENSOR)
    {
      lcd.setCursor(5, 1);
      lcd.print(F("     "));
      lcd.setCursor(5, 1);
      lcd.print(temp);
      lcd.setCursor(5, 2);
      lcd.print(F("     "));
      lcd.setCursor(5, 2);
      lcd.print(hum);
      lcd.setCursor(5, 3);
      lcd.print(F("     "));
      lcd.setCursor(5, 3);
      if (vol < SILO_LOWER_LIMIT)
      {
        digitalWrite(LOW_QTY_BUZZER_PIN, LOW);
        lcd.print("DANGER");
      }
      else if (vol >= SILO_LOWER_LIMIT && vol < SILO_UPPER_LIMIT)
      {
        digitalWrite(LOW_QTY_BUZZER_PIN, HIGH);
        lcd.print("OK");
      }
      else if (vol >= SILO_UPPER_LIMIT)
      {
        lcd.print("GOOD");
        digitalWrite(LOW_QTY_BUZZER_PIN, HIGH);
      }
    }

    if (vol < SILO_LOWER_LIMIT)
    {
      digitalWrite(LOW_QTY_BUZZER_PIN, LOW);
    }
    else if (vol >= SILO_LOWER_LIMIT && vol < SILO_UPPER_LIMIT)
    {
      digitalWrite(LOW_QTY_BUZZER_PIN, HIGH);
    }
    else if (vol >= SILO_UPPER_LIMIT)
    {
      digitalWrite(LOW_QTY_BUZZER_PIN, HIGH);
    }
  }
}
void sendDacSignal(int value)
{
  Wire.beginTransmission(PCF8591);
  Wire.write(0x40);
  Wire.write(value);
  Wire.endTransmission();

  int RawValue0 = 0;
  int DACout = 0;
  float DACoutVolt = 0.0;
  float Voltage = 0.0;
  float Offset = 0.0;
  int Ain = 0;

  DACout = value;
  RawValue0 = analogRead(Ain);
  Voltage = (RawValue0 * 5.0) / 1024.0;
  DACoutVolt = ((DACout * 5.0) / 256.0) - Offset;
  Offset = DACoutVolt - Voltage;

  //  Serial.print("DAC Out = ");
  //  Serial.print(DACout);
  //  Serial.print("\tDAC Target Voltage = ");
  //  Serial.print(DACoutVolt, 3);
  //  Serial.print("\tRaw ADC Value = ");
  //  Serial.print(RawValue0);
  //  Serial.print("\tVoltage = ");
  //  Serial.println(Voltage, 3);
}
String v = "";
void toledoCheckWeight()
{
}
void readWeighingValue()
{
  /* */
  // wSerial.write("R");
  if (Serial.available())
  {
    String s = Serial.readStringUntil('\r');
    String v = "";
    for (int i = 0; i < s.length(); i++)
    {
      Serial.println((int)s.charAt(i));
      if ((int)s.charAt(i) >= 48 && (int)s.charAt(i) <= 57)
      {
        v += s.charAt(i);
      }
      else
      {
        v += "0";
      }
    }

    Serial.println(v);
    String cut = v.substring(5, 10);
    Serial.println(cut);
    float c = cut.toFloat() / 100;
    Serial.println(c);
  }
  if (wSerial.available())
  {
    //    Serial.print(" INCOMING : ");
    // String v = wSerial.readStringUntil('\n');
    // lcd.clear();
    // lcd.setCursor(0, 0);
    // lcd.print(v);
    /** 
     * Timbangan Cina
     * wValue = v.substring(2, 9).toFloat();
     */
    /** 
     * Timbangan Mettler Toledo
     * 40    018   000
     * 40----018---000
     * 48  11168   000

     */
    String s = wSerial.readStringUntil('\r');
    String v = "";
    for (int i = 0; i < s.length(); i++)
    {
      if ((int)s.charAt(i) >= 48 && (int)s.charAt(i) <= 57)
      {
        v += s.charAt(i);
      }
      else
      {
        v += "0";
      }
    }

    // Serial.println(v);
    String cut = v.substring(4, 9);
    // Serial.println(cut);
    wValue = cut.toFloat() / 100;
    // Serial.println(wValue);
    // lcd.setCursor(0, 1);
    // lcd.print(wValue);
    //    String wval = wSerial.readString();
    //    Serial.print("Weighing Value : ");
    //    Serial.println(wval);
  }
}
void setup()
{
  //  pinMode(PIN_TX_WEIGHING, INPUT);
  //  pinMode(PIN_RX_WEIGHING, INPUT);
  pinMode(PIN_RELAY_VOUTP, OUTPUT);
  pinMode(PIN_RELAY_VOUT, OUTPUT);
  pinMode(PIN_RELAY_AUX1, OUTPUT);
  pinMode(PIN_RELAY_AUX2, OUTPUT);

  pinMode(PIN_SPD_1, OUTPUT);
  pinMode(PIN_SPD_2, OUTPUT);
  pinMode(PIN_SPD_3, OUTPUT);
  pinMode(PIN_SPD_4, OUTPUT);
  digitalWrite(PIN_SPD_1, HIGH);
  digitalWrite(PIN_SPD_2, HIGH);
  digitalWrite(PIN_SPD_3, HIGH);
  digitalWrite(PIN_SPD_4, HIGH);

  /** Relay uses LOW trigger */
  digitalWrite(PIN_RELAY_VOUT, HIGH);
  digitalWrite(PIN_RELAY_VOUTP, HIGH);
  digitalWrite(PIN_RELAY_AUX1, HIGH);
  digitalWrite(PIN_RELAY_AUX2, HIGH);

  // put your setup code here, to run once:

  Wire.begin();

  RTC.begin(); // load the time from your computer.

  if (!RTC.isrunning())
  {
    Serial.println("RTC is NOT running!"); // This will reflect the time that your sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }

  keypad.begin(makeKeymap(keys));

  lcd.init();
  lcd.begin(20, 4);
  lcd.backlight();

  wSerial.begin(9600);
  Serial.begin(9600);
  lcd.setCursor(0, 0);
  lcd.print(F(" ------------------"));
  lcd.setCursor(0, 1);
  lcd.print(F("|    Marva Auto    |"));
  lcd.setCursor(0, 2);
  lcd.print(F("|   Weighing 1.0   |"));
  lcd.setCursor(0, 3);
  lcd.print(F(" ------------------"));
  // delay(2000);

  // int diagnosticKeypad = i2cDiagnostic(32);
  // int diagnosticLCD = i2cDiagnostic(63);
  // lcd.setCursor(0, 0);
  // lcd.print(F("----- DAGNOSTIC ----"));
  // lcd.setCursor(0, 1);
  // if (diagnosticLCD == 0)
  // {
  //   lcd.print(F("LCD OK"));
  // }
  // else
  // {
  //   lcd.print(F("LCD NOT FOUND"));
  // }
  // lcd.setCursor(0, 2);
  // if (diagnosticKeypad == 0)
  // {
  //   lcd.print(F("KEYPAD OK"));
  // }
  // else
  // {
  //   lcd.print(F("KEYPAD NOT FOUND"));
  // }

  // Adjusting LAST TIME MILIS to NOW
  matNameBuffer = getMatName();
  Serial.print(F("Assigned MATNAME : "));
  Serial.println(matNameBuffer);

  fetchSettings();

  time_prev = millis();

  delay(2000);

  setupTimer();

  screen = SCREENTYPE_HOME;
  drawScreen();
}
void fetchSettings()
{
  return;
  float savedStopDelta = getStopDelta();

  Serial.print(F("Saved Stop Delta : "));
  Serial.println(savedStopDelta);
  if (savedStopDelta != 0.0f)
  {
    stop_delta = savedStopDelta;
  }

  float savedScaleOffset = getScaleOffset();

  Serial.print(F("Saved Scale Offset : "));
  Serial.println(savedScaleOffset);
  if (savedScaleOffset != 0.0f)
  {
    mSpeedOffset = savedScaleOffset;
  }

  float savedTolerance = getScaleTolerance();

  Serial.print(F("Saved Scale Tolerance : "));
  Serial.println(savedTolerance);
  if (savedTolerance != 0.0f)
  {
    mTolerance = savedTolerance;
  }
}
String printDate()
{
  DateTime now = RTC.now();
  char r[30];
  sprintf(r, "%02d/%02d/%02d %02d:%02d:%02d\0", now.day(), now.month(), now.year(), now.hour(), now.minute(), now.second());
  return String(r);
}

void drawScreen()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  /** Reset Blink */
  lcd.noBlink();
  tValueBuffer = "";
  switch (screen)
  {
  case SCREENTYPE_HOME:
    lcd.print(SCREEN_HOME());
    break;
  case SCREENTYPE_SETTINGS:
    lcd.print(SCREEN_SETTINGS());
    break;
  case SCREENTYPE_INFO:
    lcd.print(SCREEN_INFO());
    break;
  case SCREENTYPE_TIMBANG:
    /* do nothing */
    break;
  case SETTINGS_DATE:
    lcd.print(SCREEN_SETTINGS_TIME(printDate()));
    lcd.setCursor(0, 2);
    lcd.blink();
    break;
  case SETTINGS_MATNAME:
    lcd.print(SCREEN_SETTINGS_MATNAME(matNameBuffer));
    lcd.setCursor(matNameBuffer.length() + 1, 1);
    lcd.blink();
    break;
  case SETTINGS_SCALE_LIMIT:
    // SCREEN_SETTINGS_SCALE(stop_delta, mSpeedOffset, mTolerance, *lcd);
    lcd.print(F("Scale Set    [*]Back"));
    lcd.setCursor(0, 1);
    lcd.print(F("Toleransi : "));
    lcd.print(String(mSpeedOffset));
    lcd.setCursor(0, 2);
    lcd.print(F("Offset    : "));
    lcd.print(String(stop_delta));
    lcd.setCursor(0, 3);
    lcd.print(F("Limit     : "));
    lcd.print(String(mTolerance));
    // lcd.setCursor(8, 1);
    // cursorX = 8;
    // cursorY = 1;
    // // lcd.blink();
    // tBefore = 0;
    break;
  case SCREENTYPE_INFO_BAUDRATE:
    lcd.print(SCREEN_INFO_BAUDRATE());
    break;
  case SCREENTYPE_INFO_SENSOR:
    lcd.print(SCREEN_INFO_SENSOR());
    break;
  case SCREENTYPE_MANUAL_MODE:
    lcd.print(SCREEN_MANUAL_MODE());
    break;
  }
}
float getPercentage()
{
  return wValue / tValue * 100;
}
String printQty(float v)
{
  String w = String(v);
  if (v < 10)
  {
    w = "0" + w;
  }

  return w;
}
void printStat()
{
  if (autoScale)
  {
    float delta = 0.02 * tValue;
    Serial.print(F("Autoscale add "));
    wValue += delta;
    Serial.print(delta);
    Serial.print(F(" => "));
    Serial.println(wValue);
  }
  if (!statPrinted)
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Target  : ");

    lcd.print(printQty(tValue + mTolerance));
    lcd.print(" KG");

    lcd.setCursor(0, 2);
    lcd.print("      ");
    lcd.print(matNameBuffer);

    lcd.setCursor(0, 1);
    lcd.print("Current : ");

    lcd.setCursor(0, 3);
    lcd.print("[");
    lcd.setCursor(19, 3);
    lcd.print("]");

    statPrinted = true;
  }

  int percentage = int(getPercentage());
  float bar = (float)percentage / (100.0f / 18.0f);

  char buffer[10];

  lcd.setCursor(10, 1);
  lcd.print(printQty(wValue));
  lcd.print(" KG");

  lcd.setCursor(1, 3);
  lcd.print("                  ");
  lcd.setCursor(1, 3);

  for (int i = 0; i < int(bar); i++)
  {
    lcd.print("=");
  }

  sprintf(buffer, "%3d%% ", percentage);
  lcd.setCursor(8, 3);
  lcd.print(buffer);
}
int manualGear = 0;
void processInputForManualMode(char input)
{
  if (input == '*')
  {
    motorSpeed = 0;
    sendMotorSpeed();
    screen = SCREENTYPE_HOME;
    drawScreen();
  }
  if (input == 'A')
  {
    if (motorSpeed < 1)
    {
      switch (manualGear)
      {
      case 0:
        motorSpeed = LIMIT0;
        manualGear++;
        break;
      case 1:
        motorSpeed = LIMIT1;
        manualGear++;
        break;
      case 2:
        motorSpeed = LIMIT2;
        manualGear++;
        break;
      case 3:
        motorSpeed = 1;
        break;
      default:
        break;
      }
    }
    sendMotorSpeed();
  }
  else if (input == 'B')
  {
    if (motorSpeed > 0)
    {
      switch (manualGear)
      {
      case 3:
        motorSpeed = LIMIT2;
        manualGear--;
        break;
      case 2:
        motorSpeed = LIMIT1;
        manualGear--;
        break;
      case 1:
        motorSpeed = LIMIT0;
        manualGear--;
        break;
      case 0:
        motorSpeed = 0;
        break;
      default:
        break;
      }
    }
    sendMotorSpeed();
  }
}
void processInputForMatName(char input)
{
  if (input == '#')
  {
    if (matNameBuffer.length() > 0)
    {
      lcd.setCursor(0, 1);
      lcd.print("                    ");
      matNameBuffer = matNameBuffer.substring(0, matNameBuffer.length() - 1);
      lcd.setCursor(0, 1);
      lcd.print(matNameBuffer);
    }
    return;
  }
  if (input == 'D')
  {
    setMatName(matNameBuffer);
    lcd.clear();
    lcd.setCursor(0, 1);
    lcd.print(F("     Tersimpan!     "));
    lcd.noBlink();
    screen = SCREENTYPE_HOME;
    delay(3000);
    drawScreen();

    return;
  }
  if (input == '*')
  {
    lcd.noBlink();
    screen = SCREENTYPE_SETTINGS;
    drawScreen();
    return;
  }
  if (matNameBuffer.length() >= 8)
    return;
  matNameBuffer += input;
  lcd.setCursor(0, 1);
  lcd.print(matNameBuffer);
}
void processInputForInfo(char input)
{
  if (input == '*')
  {
    screen = SCREENTYPE_SETTINGS;
    drawScreen();
    return;
  }
  if (input == 'A')
  {
    screen = SCREENTYPE_INFO_BAUDRATE;
    drawScreen();
    return;
  }
  if (input == 'B')
  {
    screen = SCREENTYPE_INFO_SENSOR;
    drawScreen();
  }
}

void processInputForDate(char input)
{
  /** Input setting tanggal */
  // 10/05/2019 18:38:00
  if (input == '*')
  {
    screen = SCREENTYPE_SETTINGS;
    drawScreen();
    return;
  }
  if (input == 'A')
  {
    if (tValueBuffer.length() < 19)
    {
      return;
    }
    short dt = tValueBuffer.substring(0, 2).toInt();
    short mn = tValueBuffer.substring(3, 5).toInt();
    short yr = tValueBuffer.substring(6, 11).toInt();
    short hr = tValueBuffer.substring(11, 13).toInt();
    short min = tValueBuffer.substring(14, 16).toInt();
    short sc = tValueBuffer.substring(17, 20).toInt();

    // setTime(hr, min, sec, day, month, yr);

    RTC.adjust(DateTime(yr, mn, dt, hr, min, sc));

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Tersimpan!");
    lcd.setCursor(0, 1);
    lcd.print(printDate());
    delay(2000);
    screen = SCREENTYPE_HOME;

    drawScreen();
  }
  if (tValueBuffer.length() >= 19)
  {
    lcd.setCursor(0, 3);
    lcd.print("A<Save");
  }
  tValueBuffer += input;
  lcd.setCursor(0, 2);
  lcd.print(tValueBuffer);
  if (tValueBuffer.length() == 2)
  {
    // Move to date
    tValueBuffer += "/";
    lcd.setCursor(0, 2);
    lcd.print(tValueBuffer);
    lcd.setCursor(3, 2);
  }
  if (tValueBuffer.length() == 5)
  {
    // Move to month
    tValueBuffer += "/";
    lcd.setCursor(0, 2);
    lcd.print(tValueBuffer);
    lcd.setCursor(6, 2);
  }
  if (tValueBuffer.length() == 10)
  {
    // Move to year
    tValueBuffer += " ";
    lcd.setCursor(0, 2);
    lcd.print(tValueBuffer);
    lcd.setCursor(11, 2);
  }
  if (tValueBuffer.length() == 13)
  {
    // Move to year
    tValueBuffer += ":";
    lcd.setCursor(0, 2);
    lcd.print(tValueBuffer);
    lcd.setCursor(14, 2);
  }
  if (tValueBuffer.length() == 16)
  {
    // Move to year
    tValueBuffer += ":";
    lcd.setCursor(0, 2);
    lcd.print(tValueBuffer);
    lcd.setCursor(17, 2);
  }
}
void processInputForTime(char input)
{
  /** Input setting jam */
}
void processInputForHome(char input)
{
  if (input == 'A')
  {
    screen = SCREENTYPE_TIMBANG;
    startInputQty();
  }
  if (input == 'B')
  {
    screen = SCREENTYPE_SETTINGS;
    drawScreen();
  }
  if (input == 'C')
  {
    screen = SCREENTYPE_INFO;
    drawScreen();
  }
  if (input == 'D')
  {
    // TODO : TEST PRINTER
  }
  if (input == '#')
  {
    screen = SCREENTYPE_MANUAL_MODE;
    drawScreen();
  }
  // drawScreen();
}
void processInputForSettings(char input)
{
  if (input == 'A')
  {
    //Setting time
    screen = SETTINGS_DATE;
  }
  if (input == 'B')
  {
    //Setting scale offset
    screen = SETTINGS_SCALE_LIMIT;
  }
  if (input == 'C')
  {
    // Mat
    screen = SETTINGS_MATNAME;
  }
  if (input == '*')
  {
    screen = SCREENTYPE_HOME;
  }
  drawScreen();
}
void processInputForScaleLimit(char input)
{
  if (input == '*')
  {

    screen = SCREENTYPE_SETTINGS;
    lcd.noBlink();
    drawScreen();
    return;
  }
  if (input == 'D')
  {
    //Next
    if (tBefore == 0)
    {
      cursorX = 5;
      cursorY = 2;
      mSpeedOffset = tValueBuffer.toFloat();
      lcd.setCursor(5, 2);
      tValueBuffer = "";
      tBefore = 1;
    }
    if (tBefore == 1)
    {
      cursorX = 15;
      cursorY = 2;
      stop_delta = tValueBuffer.toFloat();
      lcd.setCursor(15, 2);
      tValueBuffer = "";
      tBefore = 2;
    }
    if (tBefore == 3)
    {
      cursorX = -1;
      cursorY = 15;
      mTolerance = tValueBuffer.toFloat();
      lcd.noBlink();
      tValueBuffer = "";
      tBefore = 2;
    }
  }
  if (input == 'A')
  {
    return;
    //Save
    setScaleOffset(mSpeedOffset);
    setStopDelta(stop_delta);
    setScaleTolerance(mTolerance);

    fetchSettings();
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(F("Scale offset : "));
    lcd.print(mSpeedOffset);
    lcd.setCursor(0, 1);
    lcd.print(F("Scale limit : "));
    lcd.print(stop_delta);
    lcd.setCursor(0, 2);
    lcd.print(F("Tolerance : "));
    lcd.print(mTolerance);
    tBefore = 0;
    tValueBuffer = "";

    delay(1000);
    screen = SCREENTYPE_HOME;
    drawScreen();
  }

  if (input == '#')
  {
    input = '.';
  }
  if (cursorX == -1)
    return;
  tValueBuffer += input;
  lcd.setCursor(cursorX, cursorY);
  lcd.print("     ");
  lcd.setCursor(cursorX, cursorY);
  lcd.print(tValueBuffer);
}
void invokeSerialCommand(String sdata)
{
  // String sdata = Serial.readStringUntil('\n');
  // Simulating button press, send k:<char>
  if (sdata.indexOf("k:") >= 0)
  {
    charInputBuffer = sdata.substring(2).charAt(0);
  }
  // Simulating weighing value, send w:<weighing value>
  if (sdata.indexOf("w:") >= 0)
  {
    wValue = sdata.substring(2).toFloat();
  }
  if (sdata.indexOf("a:") >= 0)
  {
    dacVal = sdata.substring(2).toFloat();

    sendDacSignal(dacVal);
  }
  // if (sdata.indexOf("m:") >= 0)
  // {
  //   motorSpeed = sdata.substring(2).toFloat();

  //   sendMotorSpeed();
  // }
  if (sdata.indexOf("winfo") >= 0)
  {
    printWinfo = !printWinfo;
    Serial.print("Print Winfo = ");
    Serial.println(printWinfo);
  }
  if (sdata.indexOf("va:") >= 0)
  {
    for (int i = 0; i < 256; i++)
    {
      sendDacSignal(i);
    }
  }
  if (sdata.indexOf("autoscale") >= 0)
  {
    autoScale = true;
  }
  if (sdata.indexOf("now!!") >= 0)
  {
    Serial.println(printDate());
  }
  if (sdata.indexOf("com:") >= 0)
  {
    slaveComm(sdata.substring(4));
  }
}
bool manualButtonKeyHeld = false;
void processInputs()
{
  // blinkCursor();
  // For testing. Use serial instead of buttons to send data
  if (Serial.available())
  {
    invokeSerialCommand(Serial.readStringUntil('\n'));
  }
  char input = getInput();

  if (input == '\r')
    return;

  if (screen == SCREENTYPE_HOME)
  {
    processInputForHome(input);
  }
  else if (screen == SCREENTYPE_TIMBANG)
  {
    if (acceptingTargetInput)
    {
      if (input == 'A')
      {
        // Button to begin weighing
        tValue = tValueBuffer.toFloat();
        if (tValue == 0)
          return;
        acceptingTargetInput = false;

        //Minus tolerance
        tValue -= mTolerance;
        tValue = tValue < 0 ? 0 : tValue;
        tValueBuffer = "";

        stop_percentage = 1 - (stop_delta / tValue);
        Serial.print("Stop Percentage : ");
        Serial.println(stop_percentage);
        isStart = true;
        motorSpeed = 0;

        // Print initial stat
        printStat();
      }
      else if (input == 'B')
      {
        // Button to delete input
        backspace();
      }
      else if (input != 'A' && input != 'B' && input != 'C' && input != 'D' && input != '#')
      {
        // Button to input
        // lcd.blink();
        addInput(input);
      }
    }
    else
    {
      //Not accepting target input
    }
  }
  else if (screen == SCREENTYPE_SETTINGS)
  {
    processInputForSettings(input);
  }
  else if (screen == SETTINGS_DATE)
  {
    processInputForDate(input);
  }
  else if (screen == SETTINGS_TIME)
  {
    processInputForTime(input);
  }
  else if (screen == SETTINGS_SCALE_LIMIT)
  {
    processInputForScaleLimit(input);
  }
  else if (screen == SETTINGS_MATNAME)
  {
    processInputForMatName(input);
  }
  else if (screen == SCREENTYPE_TIMBANG_DONE)
  {
    if (input == 'A')
    {
      screen = SCREENTYPE_HOME;
      drawScreen();
    }
  }
  else if (screen == SCREENTYPE_INFO)
  {
    processInputForInfo(input);
  }
  else if (screen == SCREENTYPE_INFO_BAUDRATE || screen == SCREENTYPE_INFO_SENSOR)
  {
    if (input == '*')
    {
      screen = SCREENTYPE_INFO;
      drawScreen();
    }
  }
  else if (screen == SCREENTYPE_MANUAL_MODE)
  {
    processInputForManualMode(input);
  }
}
void backspace()
{
  lcd.setCursor(0, 2);
  // Clear line one
  lcd.print("       ");
  lcd.setCursor(0, 2);
  if (tValueBuffer.length() > 0)
    tValueBuffer = tValueBuffer.substring(0, tValueBuffer.length() - 1);
  lcd.print(tValueBuffer);
}
void addInput(char input)
{
  lcd.setCursor(0, 2);
  if (input == '*')
  {
    input = '.';
  }
  if (tValueBuffer.indexOf(".") > 0 && input == '.')
  {
    return;
  }
  if (tValueBuffer.length() < 5)
  {
    tValueBuffer += input;
  }
  lcd.print(tValueBuffer);
}
void startInputQty()
{
  lcd.clear();
  lcd.setCursor(0, 0);
  tValue = 0;
  isStart = false;
  acceptingTargetInput = true;
  lcd.print(F("   MULAI TIMBANG   "));
  lcd.setCursor(0, 1);
  lcd.print(F("Input Qty Standar:"));
  lcd.setCursor(9, 2);
  lcd.print(F("KG"));
  lcd.setCursor(0, 3);
  lcd.print(F("[A]Mulai    [B]Hapus"));

  statPrinted = false;
}
void debugFunction()
{
  if (printWinfo)
  {
    Serial.print("Weight : ");
    Serial.println(wValue);
  }
}
void loop()
{
  timer.run();
  readWeighingValue();

  debugFunction();

  // GET CURRENT TIME
  time_now = millis();

  processInputs();

  if (isStart)
  {

    processWeighing();

    // Send motor speed to DAC
    sendMotorSpeed();

    if (finish)
    {
      autoScale = false;
      // delay(2000);
      // printFinish();
      timer.setTimeout(5000, printFinish);
    }
  }
  else
  {
    tBefore = time_now;
  }
  time_prev = time_now;
}
void printFinish()
{
  lcd.clear();
  lcd.setCursor(0, 0);

  lcd.print(F("   Proses Selesai"));
  lcd.setCursor(0, 1);
  lcd.print(F("QTY Akhir :"));
  lcd.print(printQty(wValue));
  lcd.print(F(" KG"));
  lcd.setCursor(0, 2);
  lcd.print(printDate());
  lcd.setCursor(0, 3);
  lcd.print(F("B<Print       Back>A"));

  finish = false;

  screen = SCREENTYPE_TIMBANG_DONE;

  motorSpeed = 0;
  sendMotorSpeed();
}
void sendMotorSpeed()
{
  /** Use DAC */
  if (motorSpeed > 0)
  {
    // digitalWrite(PIN_RELAY_VOUT, LOW);
    // digitalWrite(PIN_RELAY_VOUTP, LOW);
    digitalWrite(PIN_RELAY_AUX1, LOW);
  }
  else
  {
    // digitalWrite(PIN_RELAY_VOUT, HIGH);
    // digitalWrite(PIN_RELAY_VOUTP, HIGH);
    digitalWrite(PIN_RELAY_AUX1, HIGH);
  }
  // float mValue = motorSpeed * DAC_RESOLUTION;

  // sendDacSignal(mValue);

  /** Use Speed Relay */
  if (motorSpeed > 0 && motorSpeed <= LIMIT0)
  {
    digitalWrite(PIN_SPD_1, LOW);
  }
  else
  {
    digitalWrite(PIN_SPD_1, HIGH);
  }
  if (motorSpeed > LIMIT0 && motorSpeed <= LIMIT1)
  {
    digitalWrite(PIN_SPD_2, LOW);
  }
  else
  {
    digitalWrite(PIN_SPD_2, HIGH);
  }
  if (motorSpeed > LIMIT1 && motorSpeed <= LIMIT2)
  {
    digitalWrite(PIN_SPD_3, LOW);
  }
  else
  {
    digitalWrite(PIN_SPD_3, HIGH);
  }
  if (motorSpeed > LIMIT2)
  {
    digitalWrite(PIN_SPD_4, LOW);
  }
  else
  {
    digitalWrite(PIN_SPD_4, HIGH);
  }
}
void processWeighing()
{
  // When scale returning NOT ZERO and system is not ZEROED yet
  // After previous transaction
  if (wValue != 0.0f && zeroed == false)
  {
    long int timenow = millis();
    pTime += time_now - time_prev;
    if (pTime > printDelta)
    {
      pTime = 0;

      Serial.print(F("Waiting for rezero. Wvalue = "));
      Serial.println(wValue);

      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(F("NOLKAN"));
      lcd.setCursor(0, 1);
      lcd.print(F("TIMBANGAN"));
      lcd.setCursor(0, 2);
      lcd.print(F("QTY Sekarang : "));
      lcd.setCursor(8, 3);
      lcd.print(wValue);

      statPrinted = false;

      motorSpeed = 0;
    }

    return;
  }
  // The system is not ZEROED but the scale is ZERO
  // Proceed!
  if (zeroed == false && wValue == 0.0f)
  {
    zeroed = true;
    motorSpeed = 1;
  }
  // timenow is internal timing in weighing scope
  long int timenow = millis();
  pTime += time_now - time_prev;
  if (pTime > printDelta)
  {
    pTime = 0;
    printStat();
  }

  // Weighing value percentage
  float wPercentage = getPercentage();
  // Rate weight per milisecond avg
  // Delta percentage / delta Time
  float timeDelta = (float)(timenow - tBefore);
  //    Serial.print("DELTA : ");Serial.print(timeDelta);
  // float wRate = (wPercentage - lPercentage) / timeDelta;

  // Check whether current percentage is ABOVE stop percentage
  // is so, then we dont need to calculate stop percentage anymore
  if (wPercentage >= stop_percentage)
  {
    // Begin slowdown routine
    // Get weight rate stopping percentage
    // delta of weighing AFTER stop percentage / 100 - stop percentage
    //      float stop_adjust = (tValue - wValue) / (100 - stop_percentage);
    float stop_adjust = (wPercentage - stop_percentage) / (100 - stop_percentage);
    // stop adjust is between 0 and 1, indicating how much the weight is
    // filling remaining weight AFTER stop percentage

    // Change motor speed according to calculated value
    motorSpeed = 1 - stop_adjust;
    // if (motorSpeed <= 0.05)
    // {
    //   motorSpeed += mSpeedOffset;
    // }
  }
  else
  {
    // Check if weight rate is HIGHER than stop_percentage
    // This is to prevent overflow if somehow weight rate is above
    // stop percentage + 10% tolerance
    // Keep in mind that wRate is in MILISECONDS
    // Convert it first to second and then we got the percentage / seconds
    // Disable it for now
    //      if(wRate * 1000 > stop_percentage){
    //        // Set stop percentage TWO TIMES wRate
    //        Serial.print("Changing stop percentage to ");
    //        Serial.println(wRate * 1000 * 2);
    //        stop_percentage = wRate * 1000 * 2;
    //      }
  }

  //    Serial.print(" MSPD : ");
  //    Serial.print(motorSpeed);
  //    Serial.print(" RATE : ");
  //    Serial.print(wRate * 1000);
  //    Serial.print(" : ");
  //    Serial.println(getPercentage());
  // Set last percentage to current percentage
  lPercentage = wPercentage;
  // float ratePerSec = wRate * 1000;
  // if (wPercentage >= (100 - ratePerSec))
  // {
  //   Serial.print("STOPPING...");
  //   Serial.print(ratePerSec);
  // }

  // Stop Routine
  if (wPercentage >= 100)
  {
    printStat();
    // The weight of material vs target weight is above or equal to 100%
    // Stop motor

    tValue = 0;
    zeroed = false;
    motorSpeed = 0;
    isStart = false;
    acceptingTargetInput = false;

    finish = true;
  }
  // timenow is internal timing in weighing scope
  // tBefore is internal timing in weighing scope
  tBefore = timenow;
}
