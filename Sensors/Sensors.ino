#include <Wire.h>
#include <SHT1x.h>
#include <DHT.h>
#include <SimpleTimer.h>

#define MASTER_ADDR 8
#define DHT_PIN A0

#define SENSOR_LIMIT_LOW 6
#define SENSOR_LIMIT_HIGH 7

SimpleTimer timer;

DHT dht(DHT_PIN, DHT11);

char humidityValue[5] = "0000";
char tempValue[5] = "0000";

String cdistance;
String fwdStr;
int distance;
void processCommand(int data)
{
   Serial.print("INCOMING : ");
   String cmd;
   if (Wire.available())
   {
      cmd = Wire.readStringUntil('\n'); // receive byte as a character
   }

   Serial.println(cmd);
   Wire.beginTransmission(MASTER_ADDR);
   Wire.write("COMMOK\n");
   Wire.endTransmission();
}
void serialScan()
{
   if (Serial.available())
   {
      String cmd = Serial.readStringUntil('\n');
      if (cmd.indexOf("fwd:") >= 0)
      {
         fwdStr = cmd.substring(4);
      }
   }
}
void returnRequest()
{
   if (fwdStr.length() > 0)
   {
      Wire.write("cs:");
      for (int i = 0; i < fwdStr.length(); i++)
      {
         Wire.write(fwdStr.charAt(i));
      }
      fwdStr = "";
      return;
   }
   char buf[20];
   Wire.write("D:");
   String humidity = String(humidityValue);
   Serial.print("Sending humidity of ");
   Serial.println(humidityValue);
   String temp = String(tempValue);
   for (int i = 0; i < cdistance.length(); i++)
   {
      Wire.write(cdistance.charAt(i));
   }
   Wire.write("H:");
   for (int i = 0; i < humidity.length(); i++)
   {
      Wire.write(humidity.charAt(i));
   }
   Wire.write("T:");
   for (int i = 0; i < temp.length(); i++)
   {
      Wire.write(temp.charAt(i));
   }
   Wire.write('\n');
}
void setup()
{
   Wire.begin(8);
   Wire.onReceive(processCommand);
   Wire.onRequest(returnRequest);
   Serial.begin(9600); // Starting Serial Terminal
   dht.begin();
   timer.setInterval(300, updateSensorState);

   pinMode(SENSOR_LIMIT_LOW, INPUT);
   pinMode(SENSOR_LIMIT_HIGH, INPUT);
}

void loop()
{
   serialScan();
   cdistance = printDistance();
   updateSensorState();
   delay(1000);
}
void updateSensorState()
{
   float humidity = dht.readHumidity();
   float temp = dht.readTemperature();

   Serial.print("\nCurrent humidity = ");
   if (isnan(humidity))
   {
      Serial.println("Reading humidity failed");
   }
   else
   {
      sprintf(humidityValue, "%04d", int(humidity));
      Serial.print(humidityValue);
   }
   Serial.print("temperature = ");
   if (isnan(temp))
   {
      Serial.println("Reading temperature failed");
   }
   else
   {
      Serial.print(temp);
      sprintf(tempValue, "%04d", int(temp));
   }
   Serial.print(" Distance : ");
   Serial.println(distance);
}
String printTemp()
{
   return String("0027");
}
String printDistance()
{
   // long duration, inches, cm;
   // pinMode(pingPin, OUTPUT);
   // digitalWrite(pingPin, LOW);
   // delayMicroseconds(2);
   // digitalWrite(pingPin, HIGH);
   // delayMicroseconds(10);
   // digitalWrite(pingPin, LOW);
   // pinMode(echoPin, INPUT);
   // duration = pulseIn(echoPin, HIGH);
   // cm = microsecondsToCentimeters(duration);
   // distance = cm;
   // cm -= 90; // Fault tolerance 90mm
   // Serial.print("Distance : ");
   // Serial.println(cm);
   int cm = 0;
   if (digitalRead(SENSOR_LIMIT_LOW) == LOW)
   {
      cm = 50;
   }
   if (digitalRead(SENSOR_LIMIT_HIGH) == LOW)
   {
      Serial.println("LOW");
      cm = 70;
   }
   else
   {
      Serial.println("HIGH");
   }
   distance = cm;
   char buf[5];
   sprintf(buf, "%05d", cm);

   return String(buf);
}

long microsecondsToInches(long microseconds)
{
   return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds)
{
   return microseconds / 29 / 2;
}
